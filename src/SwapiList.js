import React from 'react';

import PlanetList from './PlanetList';

class SwapiList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};
        
    
        this.handleChange = this.handleChange.bind(this);
      }

    handleChange(event) {
        console.log("SwapiList :: handleChange - " + event.target.value);
        this.setState({value: event.target.value});
        // searchPlanets(this.state);
/*
        fetch('https://swapi.co/api/planets?search=a')
            .then(res => res.json())
            .then((data) => {
                console.log(data);
                this.setState({ planets: data });
            })
            .catch(console.log);
*/

// TODO valor atrasado
console.log(this.state.value);

        var proxyUrl = "https://cors-anywhere.herokuapp.com/",
        targetUrl = "https://swapi.co/api/planets?search=";
        fetch(proxyUrl + targetUrl + this.state.value)
          .then(blob => blob.json())
          .then(data => {
            console.table(data);
            this.setState({ swapiResponse: data });
            // document.querySelector("pre").innerHTML = JSON.stringify(data, null, 2);
          })
          .catch(e => {
            console.log(e);
            return e;
          });
    }

    render() {
        return (


        <div id="content">
            <header>
                <h2 className="page_title">Search Planets</h2>
            </header>
            <div className="content-inner">
            
                <div className="row">
                    <div className="col-lg-12">
                        <div className="panel panel-default">
                            <div className="panel-heading">Search Planets</div>
                            <div className="panel-body">
                                    
                                <div className="form-group">
                                    
                                    <input type="text" className="form-control" value={this.state.value} onChange={this.handleChange} />
                                
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12">
                    <PlanetList swapiResponse={this.state.swapiResponse}/>
                    </div>
                </div>


            </div>
        </div>

        );
    }
}

export default SwapiList;