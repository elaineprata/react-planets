import React from 'react';
import logo from './logo.svg';
import './App.css';
import SwapiList from './SwapiList';


function App() {
  return (
    

    <div className="container-fluid display-table">
      <div className="row display-table-row">
        <div className="col-md-2 col-sm-1 hidden-xs display-table-cell valign-top" id="side-menu">            
        MENU
        </div>

      <div className="col-md-10 col-sm-11 display-table-cell valign-top box">
        <div className="row">
          <header id="nav-header" className="clearfix">
	          <div className="col-md-5">HEADER</div>
          </header>

          <SwapiList />
        </div>
      </div>
      
      
    </div>
    </div> 
  );
}

export default App;
