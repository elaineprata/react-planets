import React from 'react';


class PlanetList extends React.Component {

    render() {
        if (this.props.swapiResponse) {
            console.log('retorno: ' + this.props.swapiResponse);
            console.log('count: ' + this.props.swapiResponse.count);

            var swapiResponse = this.props.swapiResponse;

            if (swapiResponse.count > 0) {

                var planets = swapiResponse.results.map(function (planet) {
                    return (
                        <tr>
                            <td>{planet.name}</td>
                            <td></td>
                        </tr>
                    );
                });
                return <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="th-sm">Name</th>
                    <th class="th-sm">Position</th>
                </tr>
                </thead>
                <tbody>{planets}</tbody></table>;
    
            }
        }
        return null;
    }
};

export default PlanetList;